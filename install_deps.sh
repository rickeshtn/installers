!apt-get update
!apt-get install -y wget bzip2
!apt-get install -y gcc perl git
!rm -rf /var/lib/apt/lists/*

!cd /
!mkdir anaconda
!cd anaconda
!wget https://repo.anaconda.com/archive/Anaconda2-5.2.0-Linux-x86_64.sh
!bash Anaconda2-5.2.0-Linux-x86_64.sh
!source ~/.bashrc
!cd ..
!rm -r anaconda
